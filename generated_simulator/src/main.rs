mod generated_model;

use clap;
use generated_model::*;
use std::fs::File;

//use std::fs::File;
use std::io::prelude::*;

use indicatif::ProgressBar;
use rand::prelude::*;
use rand_distr::{Distribution, Exp};
use std::time::Instant;

use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[clap(short, long)]
    until: f64,
    #[clap(short, long)]
    num_obs: Option<usize>,

    #[clap(short,long)]
    visual : bool
}

pub enum StepResult {
    AllGood,
    NoReactionPossible,
    JustOutput,
}

impl generated_model::GeneratedModel {
    #[cfg(feature = "linear")]
    fn step(&mut self, step_limit: Option<f64>) -> StepResult {
        let prop_sum : f64= self.propensities.iter().sum();

        //let exp = Exp::new(prop_sum).unwrap();
        let timestep : f64 = /*1./prop_sum;*/ self.rng.sample::<f64,_>(rand_distr::Exp1)/prop_sum;
        //println!("timestep {timestep}");
        self.time += timestep;
        match step_limit {
            None => {}
            Some(x) => {
                if x < self.time {
                    self.time = x;
                    return StepResult::JustOutput;
                }
            }
        }

        //Warning: This is not 100% numerically correct ... but faster ;)
        //let rand_val: f64 = self.rng.gen_range(0.0..prop_sum); <- better Version
        let rand_val: f64 = prop_sum * self.rng.gen::<f64>(); // faster version

        let idx_of_reaction = match self.select_from_threshold(rand_val) {
            None => return StepResult::NoReactionPossible,
            Some(y) => y,
        };
        //let idx_of_reaction = dist.sample(&mut self.rng);
        //println!("{:?}", self.species_counts);
        self.stepnum += 1;
        self.fire_idx(idx_of_reaction);
        StepResult::AllGood
    }

    #[cfg(feature = "tree")]
    fn step(&mut self, step_limit: Option<f64>) -> StepResult {
        if self.propensity_tree[0] <= 0.0 {
            return StepResult::NoReactionPossible;
        }

        //let exp = Exp::new(self.propensity_tree[0]).unwrap();
        let timestep = self.rng.sample::<f64,_>(rand_distr::Exp1)/self.propensity_tree[0];
        //println!("timestep {timestep}");
        self.time += timestep;
        match step_limit {
            None => {}
            Some(x) => {
                if x < self.time {
                    self.time = x;
                    return StepResult::JustOutput;
                }
            }
        }
        let rand_val: f64 = self.rng.gen_range(0.0..self.propensity_tree[0]);
        let idx_of_reaction = match self.select_from_threshold(rand_val) {
            None => return StepResult::NoReactionPossible,
            Some(y) => y,
        };
        //println!("{:?}", self.species_counts);
        self.stepnum += 1;
        self.fire_idx(idx_of_reaction);

        StepResult::AllGood
    }
    #[cfg(feature = "linear")]
    fn rebuild_tree(&self) {

    }
    #[cfg(feature = "tree")]
    fn rebuild_tree(&mut self) {
        let mut sum = 0.;
        for (idx, p) in self.propensities.iter().enumerate().rev() {
            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;
            if right_child < self.propensity_tree.len() {
                self.propensity_tree[idx] =
                    self.propensity_tree[right_child] + self.propensity_tree[left_child];
            } else if left_child < self.propensity_tree.len() {
                self.propensity_tree[idx] = self.propensity_tree[left_child];
            } else {
                self.propensity_tree[idx] = 0.;
            }
            sum += p;
            self.propensity_tree[idx] += p;
        }
    }
    #[cfg(feature = "linear")]
    fn select_from_threshold(&mut self, rand_val: f64) -> Option<usize> {
        match self
            .propensities
            .iter()
            .scan(0.0, |sum, p| {
                *sum += p;
                Some(*sum)
            })
            .enumerate()
            .find(|(_idx, sum)| *sum > rand_val)
        {
            None => panic!("reaction finding problem"),
            Some((idx, _)) => Some(idx),
        }
    }

    #[cfg(feature = "tree")]
    fn select_from_threshold(&mut self,mut thrshold: f64) -> Option<usize> {
        /*let sum =
        match self.propensity_tree.iter().next() {
            None => return None,
            Some(x) => {
                if *x <= 0. {
                    return None;
                }
                sum;
            }
        };*/

        //let mut thrshold = rand_val * sum;
        let mut idx = 0;
        loop {
            let y = match self.propensities.get(idx) {
                None => break None,
                Some(&y) => y,
            };
            if y >= thrshold {
                break Some(idx);
            } else {
                thrshold -= y;
            }

            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;

            match self.propensity_tree.get(left_child) {
                None => break None,
                Some(&y) => {
                    if y >= thrshold {
                        idx = left_child;
                    } else {
                        thrshold -= y;
                        idx = right_child;
                    }
                }
            }
        }
    }
    fn give_metrics(&self) -> serde_json::Value {
        serde_json::json!({
            //"average_degree" : degree_sum as f64 / self.propensities.len() as f64,
            "num reac" : self.propensities.len(),
            "num species" : self.species_amounts.len(),
            "step_num" : self.stepnum,
            "mode" : if cfg!(feature = "linear") { "linear"} else {assert!(cfg!(feature = "tree"));"tree"}
            //"average_reaction_idx" : self.search_depth as f64 / self.stepnum as f64,
            //"average_num_updated" : self.num_propensity_calculations as f64 / self.stepnum as f64,
        })
    }
}
fn main() {
    let args = Args::parse();
    //let io_step = args.until / args.num_obs as f64;

    println!("SETTING {:?}!", args);

    let mut model = GeneratedModel::new();
    model.rebuild_tree();
    let pb = ProgressBar::new(1000);
    let mut num_stebs: usize = 0;
    let start = Instant::now();
    let mut observations: Vec<(f64, Vec<usize>)> = vec![];
    if args.num_obs.is_some() {
        observations.push((model.time, model.species_amounts.iter().copied().collect()));
    }
    while model.time < args.until {
        //println!("{}",model.time);
        let step_res: StepResult;
        let steplim = match args.num_obs {
            None => Some(args.until),
            Some(x) => Some(args.until / x as f64 * observations.len() as f64),
        };
        step_res = model.step(steplim); //Some(io_step));

        match step_res {
            StepResult::AllGood => {}
            StepResult::NoReactionPossible => {
                println!("No reaction possible at t = {}", model.time);
                break;
            }
            StepResult::JustOutput => {
                observations.push((model.time, model.species_amounts.iter().copied().collect()));
            }
        }
        if args.visual && num_stebs % 100000 == 0 {
            pb.set_position((model.time / args.until * 1000.) as u64);
        }
        num_stebs += 1;
    }
    if args.visual {
        pb.finish();
    }
    let time = start.elapsed().as_secs_f64();
    println!(
        "{} steps in {} seconds\n{} steps per second",
        num_stebs,
        time,
        num_stebs as f64 / time
    );
    let mut metric = model.give_metrics();
    metric["exec_time"] = serde_json::json!(time);
    metric["sim_endtime"] = serde_json::json!(model.time);
    if args.num_obs.is_some() {
        assert_eq!(args.num_obs.unwrap() + 1, observations.len());
        let mut file = File::create("minimal_ssa_output.csv").unwrap();
        for (t, vals) in observations {
            writeln!(
                file,
                "{t},{}",
                vals.iter()
                    .map(|v| format!("{v}"))
                    .collect::<Vec<_>>()
                    .join(",")
            )
            .unwrap();
        }
    }
    println!("{metric}");
}
