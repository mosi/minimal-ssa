use crate::interface::*;
use crate::methods::{Simulator, StepResult};
use rand::prelude::*;
use rand_distr::{Distribution, Exp};
//use std::collections::BTreeSet;
use fixedbitset::FixedBitSet;
//use rayon::prelude::*;

struct _DMConfig {
    dependency_graph: bool,
}

pub(crate) struct MLRulesMethod {
    propensities: Vec<f64>,
    species_counts: Vec<usize>,
    desc: ModelDescription,
    pub(crate) time: f64,
    dependency_graph: Vec<Vec<usize>>,
    dependend_propensities_in_triee: Vec<Vec<usize>>,
    propensitiy_tree: Vec<f64>,
    rng: SmallRng,
    search_depth: usize,
    //record_additional_stats: bool,
    stepnum: usize,
    num_propensity_calculations: usize,
}
impl MLRulesMethod {
    fn execute_reaction(&mut self, idx: usize) {
        for in_idx in self.desc.reactions[idx].inp.iter() {
            assert!(self.species_counts[*in_idx] > 0);
            self.species_counts[*in_idx] -= 1;
        }
        for out_idx in self.desc.reactions[idx].out.iter() {
            self.species_counts[*out_idx] += 1;
        }
    }

    fn rebuild_tree(&mut self) {
        let mut sum = 0.;
        for (idx, p) in self.propensities.iter().enumerate().rev() {
            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;
            if right_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[idx] =
                    self.propensitiy_tree[right_child] + self.propensitiy_tree[left_child];
            } else if left_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[idx] = self.propensitiy_tree[left_child];
            } else {
                self.propensitiy_tree[idx] = 0.;
            }
            sum += p;
            self.propensitiy_tree[idx] += p;
        }
    }
    fn select_from_threshold(&mut self, first_try: bool, rand_val: f64) -> Option<usize> {
        let sum;
        match self.propensitiy_tree.iter().next() {
            None => return None,
            Some(x) => {
                if *x <= 0. {
                    return None;
                }
                sum = x;
            }
        }

        let mut thrshold = rand_val * sum;
        let mut idx = 0;
        loop {
            let y = match self.propensities.get(idx) {
                None => break,
                Some(&y) => y,
            };
            if y >= thrshold {
                return Some(idx);
            } else {
                thrshold -= y;
            }

            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;

            match self.propensitiy_tree.get(left_child) {
                None => break,
                Some(&y) => {
                    if y >= thrshold {
                        idx = left_child;
                    } else {
                        thrshold -= y;
                        idx = right_child;
                    }
                }
            }
        }

        if first_try {
            //self.rebuild_tree();
            return self.select_from_threshold(false, rand_val);
        }
        if self.propensitiy_tree.len() < 100 {
            println!("Internal reaction selection error at {}! Tree problem {}!\n Tree {:?}\n{:?} \n  rand_val={}", idx, self.propensitiy_tree.len(), self.propensitiy_tree, self.propensities, rand_val);
        }
        println!(
            "Internal reaction selection error at {}! Tree problem {}!\n  rand_val={}",
            idx,
            self.propensitiy_tree.len(),
            rand_val
        );
        let rand_val = self.rng.gen_range(0.0..1.);
        return self.select_from_threshold(false, rand_val);
    }

    fn update_all_propensities(&mut self) {
        self.propensities = self
            .desc
            .reactions
            .iter()
            .map(|k| k.propensity_indexed(&self.species_counts))
            .collect();
        self.rebuild_tree();
    }
    fn update_tree_for_fired_reaction(&mut self, idx_of_reaction: usize) {
        for idx in self.dependend_propensities_in_triee[idx_of_reaction].iter() {
            let left_child = *idx * 2 + 1;
            let right_child = *idx * 2 + 2;
            if right_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[*idx] =
                    self.propensitiy_tree[right_child] + self.propensitiy_tree[left_child];
            } else if left_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[*idx] = self.propensitiy_tree[left_child];
            } else {
                self.propensitiy_tree[*idx] = 0.;
            }
            self.propensitiy_tree[*idx] += self.propensities[*idx];
        }
    }

    pub(crate) fn compute_tree_updates(dependend_reactions: &Vec<usize>) -> Vec<usize> {
        //dependend_reactions.sort();
        if dependend_reactions.is_empty() {
            return vec![];
        }
        let max_size = dependend_reactions.last().unwrap();
        let mut tree_indices_to_update = FixedBitSet::with_capacity(*max_size + 1);
        //assert!(tree_indices_to_update.count_ones(..) == 0);

        for idx in dependend_reactions.iter() {
            let mut test_idx: usize = *idx;
            while !tree_indices_to_update[test_idx] {
                tree_indices_to_update.set(test_idx, true);
                if test_idx == 0 {
                    break;
                }
                test_idx = (test_idx - 1) / 2;
            }
        }
        let mut dependend_propensities_in_triee: Vec<_> = tree_indices_to_update.ones().collect(); //.sort();
        dependend_propensities_in_triee.reverse();
        dependend_propensities_in_triee
    }
    pub fn init(desc: ModelDescription) -> Self {
        let dep_graph = desc.dependency_graph();
        MLRulesMethod {
            propensities: desc
                .reactions
                .iter()
                .map(|k| k.propensity_indexed(&desc.initial_populations))
                .collect(),
            species_counts: desc.initial_populations.clone(),
            dependend_propensities_in_triee: dep_graph
                .iter()
                .map(MLRulesMethod::compute_tree_updates)
                .collect(),
            dependency_graph: dep_graph,
            propensitiy_tree: vec![0.; desc.reactions.len()],

            desc,
            time: 0.0,
            rng: SmallRng::from_entropy(),
            //record_additional_stats: false,
            search_depth: 0,
            stepnum: 0,
            num_propensity_calculations: 0,
        }
    }
}

impl Simulator for MLRulesMethod {
    fn time(&self) -> f64 {
        self.time
    }
    fn step(&mut self, step_limit: Option<f64>) -> StepResult {
        if self.propensitiy_tree[0] <= 0.0 {
            self.update_all_propensities();
        }
        if self.propensitiy_tree[0] <= 0.0 {
            return StepResult::NoReactionPossible;
        }

        let exp = Exp::new(self.propensitiy_tree[0]).unwrap();
        let timestep = exp.sample(&mut self.rng);
        self.time += timestep;
        match step_limit {
            None => {}
            Some(x) => {
                if x < self.time {
                    self.time = x;
                    return StepResult::JustOutput;
                }
            }
        }
        let rand_val = self.rng.gen_range(0.0..1.);
        let idx_of_reaction = match self.select_from_threshold(false, rand_val) {
            None => return StepResult::NoReactionPossible,
            Some(y) => y,
        };
        self.search_depth += idx_of_reaction;
        //println!("{:?}", self.species_counts);
        self.stepnum += 1;
        self.execute_reaction(idx_of_reaction);
        for reac in self.dependency_graph[idx_of_reaction].iter() {
            self.propensities[*reac] =
                self.desc.reactions[*reac].propensity_indexed(&self.species_counts);
        }
        //if self.record_additional_stats{
        self.num_propensity_calculations += self.dependency_graph[idx_of_reaction].len();
        //}
        self.update_tree_for_fired_reaction(idx_of_reaction);

        StepResult::Ok
    }

    fn output_counts(&self) -> Vec<usize> {
        self.species_counts.clone()
    }

    fn additional_int_stats(&self) -> serde_json::map::Map<String, serde_json::Value> {
        let degree_sum: usize = self.dependency_graph.iter().map(|k| k.len()).sum();
        let k = serde_json::json!({
            "average_degree" : degree_sum as f64 / self.propensities.len() as f64,
            "num reac" : self.dependency_graph.len(),
            "num species" : self.species_counts.len(),
            "average_reaction_idx" : self.search_depth as f64 / self.stepnum as f64,
            "average_num_updated" : self.num_propensity_calculations as f64 / self.stepnum as f64,
        });
        match k {
            serde_json::Value::Object(m) => m,
            _ => {
                unreachable!()
            }
        }
    }
}
