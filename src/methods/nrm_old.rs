use crate::interface::*;
use crate::methods::{Simulator, StepResult};
use hashbrown::hash_map::DefaultHashBuilder;
use ordered_float::OrderedFloat;
use priority_queue::PriorityQueue;
use rand::prelude::*;
use rand_distr::{Distribution, Exp};
use std::cmp::Reverse;
use std::hash::{Hash, Hasher};

#[derive(Debug, Clone)]
struct QueueElement {
    reaction_idx: usize,
    //_reaction: ModelDescriptionReaction,
}
impl Eq for QueueElement {}
impl PartialEq for QueueElement {
    fn eq(&self, other: &Self) -> bool {
        self.reaction_idx.eq(&other.reaction_idx)
    }
}
impl Hash for QueueElement {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.reaction_idx.hash(state);
    }
}

pub(crate) struct NextReactionMethod {
    queue: PriorityQueue<QueueElement, Reverse<OrderedFloat<f64>>, DefaultHashBuilder>,
    species_counts: Vec<usize>,
    desc: ModelDescription,
    pub(crate) time: f64,
    dependency_graph: Vec<Vec<usize>>,
    propensities: Vec<f64>,
    current_scheduled_time: Vec<Reverse<OrderedFloat<f64>>>,
    rng: SmallRng,
}
impl NextReactionMethod {
    fn execute_reaction(&mut self, idx: usize) {
        for in_idx in self.desc.reactions[idx].inp.iter() {
            assert!(self.species_counts[*in_idx] > 0);
            self.species_counts[*in_idx] -= 1;
        }
        for out_idx in self.desc.reactions[idx].out.iter() {
            self.species_counts[*out_idx] += 1;
        }
    }
    pub fn _init(desc: ModelDescription) -> Self {
        let mut rng = SmallRng::from_entropy();
        let propensities: Vec<f64> = desc
            .reactions
            .iter()
            .map(|reac| reac.propensity_indexed(&desc.initial_populations))
            .collect();

        let first_time_points = propensities.iter().map(|prop: &f64| {
            if *prop <= 0. {
                return None;
            };
            let dist = rand_distr::Exp::new(*prop);
            match dist {
                Ok(dist) => Some(dist.sample(&mut rng)),
                Err(_) => None,
            }
        });
        let mut current_scheduled_time =
            vec![Reverse(OrderedFloat::from(-1.)); desc.reactions.len()];
        NextReactionMethod {
            queue: first_time_points
                .zip(&desc.reactions)
                .enumerate()
                .filter_map(|(r_idx, (t, _r))| match t {
                    None => None,
                    Some(t) => {
                        current_scheduled_time[r_idx] = Reverse(OrderedFloat::from(t));
                        Some((
                            QueueElement {
                                reaction_idx: r_idx,
                            },
                            Reverse(OrderedFloat::from(t)),
                        ))
                    }
                })
                .collect(),
            species_counts: desc.initial_populations.clone(),
            dependency_graph: desc
                .dependency_graph()
                .into_iter()
                .enumerate()
                .map(|(n, vec)| vec.into_iter().filter(|v| *v != n).collect())
                .collect(),
            desc,
            time: 0.0,
            current_scheduled_time,
            propensities,
            rng: rng,
        }
    }
}

impl Simulator for NextReactionMethod {
    fn time(&self) -> f64 {
        self.time
    }
    fn step(&mut self, step_limit: Option<f64>) -> StepResult {
        match self.queue.peek() {
            None => return StepResult::NoReactionPossible,
            Some((_, t)) => {
                if step_limit.is_some() && step_limit.unwrap() < t.0.into() {
                    return StepResult::JustOutput;
                } else {
                }
            }
        };

        let (ev, time): (QueueElement, Reverse<OrderedFloat<f64>>) = self.queue.pop().unwrap();

        self.time = time.0.into();
        //assert!(self.time == self.current_scheduled_time[ev.reaction_idx].0.into());
        //println!("{}",self.time);
        //println!("QUEUE {:?}", self.queue);
        self.execute_reaction(ev.reaction_idx);
        let my_new_prio =
            self.desc.reactions[ev.reaction_idx].propensity_indexed(&self.species_counts);
        self.propensities[ev.reaction_idx] = my_new_prio;
        if my_new_prio > 0. {
            let my_new_time = Exp::new(my_new_prio).unwrap().sample(&mut self.rng) + self.time;
            assert!(my_new_time > self.time);
            let my_new_time = Reverse(OrderedFloat::from(my_new_time));
            assert!(self.queue.push(ev.clone(), my_new_time).is_none());
            self.current_scheduled_time[ev.reaction_idx] = my_new_time;
        } else {
            self.current_scheduled_time[ev.reaction_idx] = Reverse(OrderedFloat::from(-1.));
        }

        for dependent_idx in self.dependency_graph[ev.reaction_idx].iter() {
            let new_prio =
                self.desc.reactions[*dependent_idx].propensity_indexed(&self.species_counts);
            let old_prio = self.propensities[*dependent_idx];

            if new_prio <= 0. && old_prio > 0. {
                self.queue.remove(&QueueElement {
                    reaction_idx: *dependent_idx,
                });
                self.current_scheduled_time[*dependent_idx] = Reverse(OrderedFloat::from(-1.));
            } else if new_prio > 0. {
                let elem = QueueElement {
                    reaction_idx: *dependent_idx,
                };
                if old_prio > 0. {
                    let old_time: f64 = self.current_scheduled_time[*dependent_idx].0.into();
                    assert!(old_time > self.time);
                    let new_time = old_prio / new_prio * (old_time - self.time) + self.time;
                    assert!(new_time > self.time);
                    let new_time = Reverse(OrderedFloat::from(new_time));
                    self.queue
                        .change_priority(&elem, new_time)
                        .expect("Change not successful");
                    self.current_scheduled_time[*dependent_idx] = new_time;
                } else {
                    let new_time = Exp::new(new_prio).unwrap().sample(&mut self.rng) + self.time;
                    assert!(new_time > self.time);
                    let new_time = Reverse(OrderedFloat::from(new_time));
                    /*println!(
                        "old propensity for {dependent_idx} was {old_prio} (after fireing {})",
                        ev.reaction_idx
                    );*/
                    assert!(self.queue.push(elem, new_time).is_none());
                    self.current_scheduled_time[*dependent_idx] = new_time;
                }
            }
            self.propensities[*dependent_idx] = new_prio;
        }
        StepResult::Ok
    }

    fn output_counts(&self) -> Vec<usize> {
        self.species_counts.clone()
    }

    fn additional_int_stats(&self) -> serde_json::map::Map<String, serde_json::Value> {
        let k = serde_json::json!({});
        match k {
            serde_json::Value::Object(m) => m,
            _ => {
                unreachable!()
            }
        }
    }
}
