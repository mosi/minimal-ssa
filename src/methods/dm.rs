use crate::interface::*;
use crate::methods::{Simulator, StepResult};
use rand::prelude::*;
use rand_distr::{Distribution, Exp};
use serde_json::Value;

pub(crate) struct DirectMethod {
    propensities: Vec<f64>,
    species_counts: Vec<usize>,
    desc: ModelDescription,
    pub(crate) time: f64,
    dependency_graph: Option<Vec<Vec<usize>>>,
    rng: SmallRng,
}
impl DirectMethod {
    fn recompute_all_propensities_in_place(&mut self) -> f64 {
        assert_eq!(self.propensities.len(), self.desc.reactions.len());
        let mut sum = 0.;
        for (p, reac) in self.propensities.iter_mut().zip(self.desc.reactions.iter()) {
            *p = reac.propensity_indexed(&self.species_counts);
            sum += *p;
        }
        sum
    }

    fn execute_reaction(&mut self, idx: usize) {
        for in_idx in self.desc.reactions[idx].inp.iter() {
            assert!(self.species_counts[*in_idx] > 0);
            self.species_counts[*in_idx] -= 1;
        }
        for out_idx in self.desc.reactions[idx].out.iter() {
            self.species_counts[*out_idx] += 1;
        }
    }
    pub fn init(desc: ModelDescription, use_dependency_graph: bool) -> Self {
        DirectMethod {
            propensities: desc
                .reactions
                .iter()
                .map(|k| k.propensity_indexed(&desc.initial_populations))
                .collect(),

            species_counts: desc.initial_populations.clone(),
            dependency_graph: match use_dependency_graph {
                true => Some(desc.dependency_graph()),
                false => None,
            },
            desc,
            time: 0.0,
            rng: SmallRng::from_entropy(),
        }
    }
}

impl Simulator for DirectMethod {
    fn step(&mut self, step_limit: Option<f64>) -> StepResult {
        let prop_sum: f64 = match self.dependency_graph {
            None => self.recompute_all_propensities_in_place(),
            Some(_) => self.propensities.iter().sum(),
        };

        if prop_sum <= 0. {
            return StepResult::NoReactionPossible;
        }
        let exp = Exp::new(prop_sum).unwrap();
        let timestep = exp.sample(&mut self.rng);
        self.time += timestep;
        match step_limit {
            None => {}
            Some(x) => {
                if x < self.time {
                    self.time = x;
                    return StepResult::JustOutput;
                }
            }
        }

        let threshold: f64 = self.rng.gen_range(0.0..prop_sum);
        let idx_of_reaction = self
            .propensities
            .iter()
            .scan(0., |acc, p| {
                *acc += *p;
                Some(*acc)
            })
            .position(|k| k > threshold);
        let idx_of_reaction = match idx_of_reaction {
            None => return StepResult::NoReactionPossible,
            Some(x) => x,
        };
        self.execute_reaction(idx_of_reaction);

        match &self.dependency_graph {
            None => {}
            Some(x) => {
                for reac in x[idx_of_reaction].iter() {
                    //println!("{idx_of_reaction} -> {reac}");
                    self.propensities[*reac] =
                        self.desc.reactions[*reac].propensity_indexed(&self.species_counts);
                }
            }
        }

        StepResult::Ok
    }

    fn output_counts(&self) -> Vec<usize> {
        self.species_counts.clone()
    }

    fn time(&self) -> f64 {
        self.time
    }

    fn additional_int_stats(&self) -> serde_json::map::Map<String, serde_json::Value> {
        let k = serde_json::json!({});
        match k {
            Value::Object(m) => m,
            _ => {
                unreachable!()
            }
        }
    }
}
