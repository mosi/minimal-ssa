use crate::interface::{ModelDescription, ModelDescriptionReaction};
use crate::methods::ml_rules::MLRulesMethod;
use askama::Template;
use hashbrown::HashSet;
use std::fs::File;
use std::io::Write;
use std::process::Command;

#[derive(Template)]
#[template(path = "code_template.txt")]
pub struct CodeGen {
    initial_populations: Vec<usize>,
    dependency_graph: Vec<Vec<usize>>,
    reactions: Vec<ModelDescriptionReaction>,
    dependend_propensities_in_triee: Vec<Vec<usize>>,
}

impl ModelDescriptionReaction {
    fn rate_to_code_gen(&self) -> String {
        let mut res = format!("{:.10e}", self.rate_constant);
        let species_set: HashSet<usize> = self.inp.iter().copied().collect();
        for s in species_set {
            let c = self.inp.iter().filter(|k| **k == s).count();
            assert!(c > 0);
            for d in 0..c {
                if d == 0 {
                    res += &*format!(" * self.species_amounts[{}] as f64", s);
                } else {
                    res += &*format!(" * (self.species_amounts[{}] - {d}) as f64", s);
                }
            }
        }
        res
    }
}

impl CodeGen {
    pub fn init(desc: ModelDescription) -> Self {
        let dep_graph = desc.dependency_graph();
        CodeGen {
            dependency_graph: desc.compute_depeendincy_graph(),
            initial_populations: desc.initial_populations,
            reactions: desc.reactions,
            dependend_propensities_in_triee: dep_graph
                .iter()
                .map(MLRulesMethod::compute_tree_updates)
                .collect(),
        }
    }

    pub fn run(desc: ModelDescription, endtime: f64, num_obs: Option<usize>) {
        let num_reacs = desc.reactions.len();
        let s = CodeGen::init(desc);
        // println!("{}",s.render().unwrap());
        let filepath = "generated_simulator/src/generated_model.rs";
        let proposed_model = s.render().unwrap();

        let old_file_contend = std::fs::read_to_string(filepath);
        let need_to_write_file = match old_file_contend {
            Ok(s) => {
                println!("existing file found");
                s != proposed_model
            }
            Err(_) => true,
        };
        if need_to_write_file {
            println!("writing new file");
            let mut file = File::create(filepath).unwrap();
            write!(file, "{}", proposed_model).unwrap();
        } else {
            println!("no need to write new file");
        }
        let status = Command::new("cargo")
            .args(&[
                "build",
                "--release",
                "--features", if num_reacs <= 8{"linear"} else {"tree"},
                "--manifest-path=generated_simulator/Cargo.toml",
            ])
            .status()
            .expect("compile failed to execute");
        if status.success() {
            match num_obs {
                None => {
                    Command::new("generated_simulator/target/release/generated_sim")
                        .arg(format!("--until={}", endtime))
                        .status()
                        .unwrap();
                }
                Some(num_obs) => {
                    Command::new("generated_simulator/target/release/generated_sim")
                        .arg(format!("--until={}", endtime))
                        .arg(format!("--num-obs={}", num_obs))
                        .status()
                        .unwrap();
                }
            }
        }
    }
}
