use crate::changable_queue;
use crate::changable_queue::ChangeableQueue;
use crate::interface::*;
use crate::methods::{Simulator, StepResult};
use ordered_float::OrderedFloat;
use rand::prelude::*;
use rand_distr::{Distribution, Exp};
use std::cmp::{max, Ordering, Reverse};
use std::fs::File;
use std::io::Write;
//use std::io::prelude::*;
use std::io::BufWriter;

#[derive(Debug, Clone)]
struct QueueElement {
    reaction_idx: usize,
    time: Reverse<OrderedFloat<f64>>,
}
impl Eq for QueueElement {}
impl PartialEq for QueueElement {
    fn eq(&self, other: &Self) -> bool {
        self.time.eq(&other.time)
    }
}

impl changable_queue::QueueElement for QueueElement {
    fn give_idx(&self) -> usize {
        self.reaction_idx
    }
}

impl PartialOrd<Self> for QueueElement {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.time.partial_cmp(&other.time)
    }
}

impl Ord for QueueElement {
    fn cmp(&self, other: &Self) -> Ordering {
        self.time.cmp(&other.time)
    }
}

pub(crate) struct NextReactionMethod2 {
    queue: ChangeableQueue<QueueElement>,
    species_counts: Vec<usize>,
    desc: ModelDescription,
    pub(crate) time: f64,
    dependency_graph: Vec<Vec<usize>>,
    propensities: Vec<f64>,
    current_scheduled_time: Vec<Reverse<OrderedFloat<f64>>>,
    rng: SmallRng,
    record_additional_stats: bool,
    critical_path_counter: Vec<usize>,
    event_counter_for_critical: usize,
    critical_path_stores: Vec<Vec<usize>>,
    file_for_event_graph: Option<BufWriter<std::fs::File>>,
}
impl NextReactionMethod2 {
    fn execute_reaction(&mut self, idx: usize) {
        for in_idx in self.desc.reactions[idx].inp.iter() {
            assert!(self.species_counts[*in_idx] > 0);
            self.species_counts[*in_idx] -= 1;
        }
        for out_idx in self.desc.reactions[idx].out.iter() {
            self.species_counts[*out_idx] += 1;
        }
    }
    pub fn init(desc: ModelDescription, record_additional_stats: bool) -> Self {
        let mut rng = SmallRng::from_entropy();
        let propensities: Vec<f64> = desc
            .reactions
            .iter()
            .map(|reac| reac.propensity_indexed(&desc.initial_populations))
            .collect();

        let first_time_points = propensities.iter().map(|prop: &f64| {
            if *prop <= 0. {
                return None;
            };
            let dist = rand_distr::Exp::new(*prop);
            match dist {
                Ok(dist) => Some(dist.sample(&mut rng)),
                Err(_) => None,
            }
        });
        let mut current_scheduled_time =
            vec![Reverse(OrderedFloat::from(-1.)); desc.reactions.len()];
        let mut queue = ChangeableQueue::new(desc.reactions.len());
        first_time_points
            .zip(&desc.reactions)
            .enumerate()
            .filter_map(|(r_idx, (t, _r))| match t {
                None => None,
                Some(t) => {
                    current_scheduled_time[r_idx] = Reverse(OrderedFloat::from(t));
                    Some(QueueElement {
                        reaction_idx: r_idx,
                        time: Reverse(OrderedFloat::from(t)),
                    })
                }
            })
            .for_each(|elem| queue.push(elem));
        NextReactionMethod2 {
            queue,
            critical_path_counter: /*match record_additional_stats{
                true => */vec![0;desc.reactions.len()],
               /* false => {vec![]}
            },*/
            critical_path_stores: match record_additional_stats{
                true => vec![vec![];desc.reactions.len()],
                false => {vec![]}
            },
            event_counter_for_critical: 0,
            species_counts: desc.initial_populations.clone(),
            dependency_graph: desc
                .dependency_graph()
                .into_iter()
                .enumerate()
                .map(|(n, vec)| vec.into_iter().filter(|v| *v != n).collect())
                .collect(),
            desc,
            time: 0.0,
            current_scheduled_time,
            propensities,
            rng: rng,
            record_additional_stats,

            file_for_event_graph: match record_additional_stats{
                true => Some(BufWriter::new(File::create("event_graph.networkx").unwrap())),
                false => None,
            },
        }
    }
}

impl Simulator for NextReactionMethod2 {
    fn time(&self) -> f64 {
        self.time
    }
    fn step(&mut self, step_limit: Option<f64>) -> StepResult {
        match self.queue.peek() {
            None => return StepResult::NoReactionPossible,
            Some(ev) => {
                if step_limit.is_some() && step_limit.unwrap() < ev.time.0.into() {
                    self.time = step_limit.unwrap();
                    return StepResult::JustOutput;
                } else {
                }
            }
        };

        //self.queue.check_heap();
        let main_ev: QueueElement = self.queue.pop().unwrap();
        assert!(self.time <= main_ev.time.0.into());
        self.time = main_ev.time.0.into();
        //println!("{}",self.time);
        //println!("QUEUE {:?}", self.queue);
        self.execute_reaction(main_ev.reaction_idx);
        self.event_counter_for_critical += 1;
        let this_ev_critical_name = self.event_counter_for_critical;
        if self.record_additional_stats {
            for oid in self.critical_path_stores[main_ev.reaction_idx].iter() {
                match self.file_for_event_graph.as_mut() {
                    None => unreachable!(),
                    Some(x) => writeln!(x, "{oid} {this_ev_critical_name}").unwrap(),
                }
            }
            self.critical_path_stores[main_ev.reaction_idx].clear();
        }
        let my_new_prio =
            self.desc.reactions[main_ev.reaction_idx].propensity_indexed(&self.species_counts);
        self.propensities[main_ev.reaction_idx] = my_new_prio;
        if self.record_additional_stats {
            self.critical_path_stores[main_ev.reaction_idx].push(this_ev_critical_name);
        }
        if my_new_prio > 0. {
            let offset = Exp::new(my_new_prio).unwrap().sample(&mut self.rng);
            let my_new_time = offset + self.time;
            assert!(offset > 0.);
            assert!(
                my_new_time >= self.time,
                "{} > {}, offset: {}, my_new_prio : {}",
                my_new_time,
                self.time,
                offset,
                my_new_prio
            );
            let my_new_time = Reverse(OrderedFloat::from(my_new_time));
            self.queue.push(QueueElement {
                reaction_idx: main_ev.reaction_idx,
                time: my_new_time,
            });
            //if self.record_additional_stats{
            self.critical_path_counter[main_ev.reaction_idx] += 1;
            //}
            self.current_scheduled_time[main_ev.reaction_idx] = my_new_time;
        } else {
            //assert_eq!(self.queue.pop().unwrap().reaction_idx, ev.reaction_idx);
            self.current_scheduled_time[main_ev.reaction_idx] = Reverse(OrderedFloat::from(-1.));
        }

        for dependent_idx in self.dependency_graph[main_ev.reaction_idx].iter() {
            self.critical_path_counter[*dependent_idx] = max(
                self.critical_path_counter[*dependent_idx],
                self.critical_path_counter[main_ev.reaction_idx],
            );
            if self.record_additional_stats {
                if self.record_additional_stats {
                    self.critical_path_stores[*dependent_idx].push(this_ev_critical_name);
                }
            }
            let new_prio =
                self.desc.reactions[*dependent_idx].propensity_indexed(&self.species_counts);
            let old_prio = self.propensities[*dependent_idx];

            if new_prio <= 0. && old_prio > 0. {
                self.queue.remove(*dependent_idx);
                self.current_scheduled_time[*dependent_idx] = Reverse(OrderedFloat::from(-1.));
            } else if new_prio > 0. {
                if old_prio > 0. {
                    let old_time: f64 = self.current_scheduled_time[*dependent_idx].0.into();
                    assert!(old_time >= self.time);
                    let new_time = old_prio / new_prio * (old_time - self.time) + self.time;
                    assert!(new_time >= self.time);
                    let new_time = Reverse(OrderedFloat::from(new_time));
                    self.queue.change_priority(QueueElement {
                        reaction_idx: *dependent_idx,
                        time: new_time,
                    });
                    self.current_scheduled_time[*dependent_idx] = new_time;
                } else {
                    let new_time = Exp::new(new_prio).unwrap().sample(&mut self.rng) + self.time;
                    assert!(new_time >= self.time);
                    let new_time = Reverse(OrderedFloat::from(new_time));
                    /*println!(
                        "old propensity for {dependent_idx} was {old_prio} (after fireing {})",
                        ev.reaction_idx
                    );*/
                    self.queue.push(QueueElement {
                        reaction_idx: *dependent_idx,
                        time: new_time,
                    });
                    self.current_scheduled_time[*dependent_idx] = new_time;
                }
            }
            self.propensities[*dependent_idx] = new_prio;
        }
        StepResult::Ok
    }

    fn output_counts(&self) -> Vec<usize> {
        self.species_counts.clone()
    }

    fn additional_int_stats(&self) -> serde_json::map::Map<String, serde_json::Value> {
        let k = serde_json::json!({
            "max_simple_critical_path" : self.critical_path_counter.iter().max()
        });
        match k {
            serde_json::Value::Object(m) => m,
            _ => {
                unreachable!()
            }
        }
    }
}
