use serde::{Deserialize, Serialize};
use std::collections::BTreeSet;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use std::mem::swap;
//use hashbrown::HashSet;
use itertools::Itertools;

#[derive(Serialize, Deserialize)]
pub struct ModelDescriptionReaction {
    pub inp: Vec<usize>,
    pub out: Vec<usize>,
    pub rate_constant: f64,
}
impl ModelDescriptionReaction {
    pub fn propensity_indexed(&self, counts: &Vec<usize>) -> f64 {
        //assert!(self.inp.is_sorted());
        let mut ret = self.rate_constant;

        let mut last_idx = usize::MAX;
        let mut counter = 0;
        for inp_idx in self.inp.iter() {
            if *inp_idx == last_idx {
                counter += 1;
                if counts[*inp_idx] > counter {
                    ret *= (counts[*inp_idx] - counter) as f64;
                } else {
                    return 0.;
                };
            } else {
                counter = 0;
                ret *= counts[*inp_idx] as f64;
                last_idx = *inp_idx;
            }
        }
        ret
    }
}

#[derive(Serialize, Deserialize)]
pub struct ModelDescription {
    pub initial_populations: Vec<usize>,
    modelname: String,
    pub reactions: Vec<ModelDescriptionReaction>,
    file_path: Option<String>,
}
impl ModelDescription {
    pub fn read_file(filename: String) -> ModelDescription {
        let mut file = File::open(filename.clone()).unwrap();
        let mut data = String::new();
        file.read_to_string(&mut data).unwrap();
        data = data.replace("\"in\"", "\"inp\"");

        let mut x: ModelDescription = serde_json::from_str(&data).unwrap();
        for r in x.reactions.iter_mut() {
            r.inp.sort();
            r.out.sort();
        }
        for r in x.reactions.iter_mut().filter(|k| k.rate_constant <= 0.) {
            r.inp.clear();
            r.out.clear();
        }
        println!(
            "Read: {}, species: {}, reactions: {}",
            x.modelname,
            x.initial_populations.len(),
            x.reactions.len()
        );
        println!("Removing zero rate reactions");
        x.reactions.retain(|k| k.rate_constant > 0.);
        println!("New number of reactions: {}", x.reactions.len());
        x.file_path = Some(filename.clone());
        let dependency_graph_path = filename + "dependency.json";
        if !std::path::Path::new(&dependency_graph_path).exists() {
            std::fs::write(
                dependency_graph_path,
                serde_json::to_string_pretty(&x.compute_depeendincy_graph()).unwrap(),
            )
            .unwrap();
        }
        x
    }
    pub fn dependency_graph(&self) -> Vec<Vec<usize>> {
        println!("Reading dependency graph from file");
        let dependency_graph_path = self.file_path.as_ref().unwrap().clone() + "dependency.json";
        let file = File::open(dependency_graph_path).unwrap();
        let reader = BufReader::new(file);
        let r = serde_json::from_reader(reader).unwrap();
        println!("Dep graph file read complete");
        r
    }
    pub fn compute_depeendincy_graph(&self) -> Vec<Vec<usize>> {
        //let num_species = self.initial_populations.len();
        //let num_reactions = self.reactions.len();

        let reaction_depends_on_species = self.reactions.iter().map(|k| {
            {
                //assert!(itertools::sorted(k.inp.iter()));
                itertools::sorted(k.inp.iter()).dedup()
            }
        });
        //.collect();

        let mut species_changed_by_reaction: Vec<Vec<usize>> =
            vec![vec![]; self.initial_populations.len()];
        for (reac_idx, reac) in self.reactions.iter().enumerate() {
            for s in reac.inp.iter().chain(reac.out.iter()) {
                if reac.inp.iter().filter(|k| **k == *s).count()
                    != reac.out.iter().filter(|k| **k == *s).count()
                {
                    species_changed_by_reaction[*s].push(reac_idx);
                }
            }
        }
        // assert this should already be the case
        /*for _s in species_changed_by_reaction.iter_mut() {
            //assert!(itertools::sorted(s.iter()));
        }*/

        let mut reaction_depends_on_reaction: Vec<BTreeSet<usize>> =
            vec![BTreeSet::new(); self.reactions.len()];
        for (number_of_reaction, reac2spec) in reaction_depends_on_species.enumerate() {
            for spec in reac2spec {
                for reac in species_changed_by_reaction[*spec].iter() {
                    reaction_depends_on_reaction[*reac].insert(number_of_reaction);
                }
            }
        }
        /*for x in reaction_depends_on_reaction.iter_mut() {
            x.sort();
            x.dedup();
        }*/
        /*println!("Dependency graph: {:?}", reaction_depends_on_reaction);
        panic!();*/
        println!("Dependency graph done");
        reaction_depends_on_reaction
            .iter()
            .map(|k| k.into_iter().cloned().collect())
            .collect()
    }

    pub fn _legacy_compute_dependency_graph(&self) -> Vec<Vec<usize>> {
        println!("Start computing dep graph");
        let reaction_depends_on_species: Vec<Vec<usize>> = self
            .reactions
            .iter()
            .map(|k| {
                let mut v: Vec<_> = k.inp.iter().cloned().collect();
                v.sort();
                v.dedup();
                v
            })
            .collect();
        let mut species_changed_by_reaction: Vec<Vec<usize>> =
            vec![vec![]; self.initial_populations.len()];
        for (reac_idx, reac) in self.reactions.iter().enumerate() {
            for s in reac.inp.iter().chain(reac.out.iter()) {
                species_changed_by_reaction[*s].push(reac_idx);
            }
        }
        for s in species_changed_by_reaction.iter_mut() {
            s.sort();
            s.dedup();
        }

        /*println!("Reaction depends on spec {:?}", reaction_depends_on_species);
        println!(
            "Species  changed by react {:?}",
            species_changed_by_reaction
        );*/
        let mut reaction_depends_on_reaction = vec![vec![]; self.reactions.len()];
        for (number_of_reaction, reac2spec) in reaction_depends_on_species.iter().enumerate() {
            for spec in reac2spec {
                for reac in species_changed_by_reaction[*spec].iter() {
                    reaction_depends_on_reaction[*reac].push(number_of_reaction);
                }
            }
        }
        for x in reaction_depends_on_reaction.iter_mut() {
            x.sort();
            x.dedup();
        }
        /*println!("Dependency graph: {:?}", reaction_depends_on_reaction);
        panic!();*/
        println!("Dependency graph done");
        //let r1 = self.compute_depeendincy_graph2();
        //assert_eq!(r1,reaction_depends_on_reaction);
        reaction_depends_on_reaction
    }
    pub fn make_next_subvolume(
        mut self,
        grid_len: usize,
        species_to_move: Option<Vec<usize>>,
        diffusion: f64,
    ) -> Self {
        let species_to_move = match species_to_move {
            None => {
                vec![]
            }
            Some(x) => x,
        };
        assert!(grid_len > 2);
        let grid_size = grid_len * grid_len;
        let grid_idxes: Vec<Vec<usize>> = (0..grid_len)
            .map(|n| ((n * grid_len)..((n + 1) * grid_len)).collect())
            .collect();
        assert_eq!(grid_idxes[0][0], 0);
        assert_eq!(grid_idxes[grid_len - 1][grid_len - 1], grid_size - 1);
        assert_eq!(grid_idxes.len(), grid_len);
        grid_idxes
            .iter()
            .for_each(|k| assert_eq!(k.len(), grid_len));

        let num_species = self.initial_populations.len();

        self.initial_populations = self
            .initial_populations
            .iter()
            .cycle()
            .take(self.initial_populations.len() * grid_size)
            .cloned()
            .collect();
        let mut reference_reactions = vec![];
        swap(&mut reference_reactions, &mut self.reactions);
        for x in 0..grid_len {
            for y in 0..grid_len {
                let offset = num_species * grid_idxes[x][y];
                self.reactions.extend(reference_reactions.iter().map(|k| {
                    ModelDescriptionReaction {
                        inp: k.inp.iter().map(|v| v + offset).collect(),
                        out: k.out.iter().map(|v| v + offset).collect(),
                        rate_constant: k.rate_constant,
                    }
                }));
                for dx in vec![-1, 0, 1] {
                    for dy in vec![-1, 0, 1] {
                        if (dx != 0) ^ (dy != 0) {
                            let x2 = ((x + grid_len) as isize + dx) as usize % grid_len;
                            let y2 = ((y + grid_len) as isize + dy) as usize % grid_len;
                            let other_offset = grid_idxes[x2][y2] * num_species;
                            //println!("{x} {y} -> {x2} {y2} {}", self.reactions.len());
                            self.reactions.extend(species_to_move.iter().map(|k| {
                                ModelDescriptionReaction {
                                    inp: vec![k + offset],
                                    out: vec![k + other_offset],
                                    rate_constant: diffusion,
                                }
                            }));
                        }
                    }
                }
            }
        }
        //println!("Total num reacs: {}",self.reactions.len());
        self
    }
}
