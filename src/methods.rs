pub mod code_gen;
pub mod dm;
pub mod ml_rules;
pub mod nrm2;
pub mod nrm_no_resch;
pub mod nrm_old;
//pub mod nrm_no_resch;

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum Method {
    Mlrules,
    Nrm,
    Dm,
    DmGraph,
    CodeGen,
}

pub enum StepResult {
    Ok,
    NoReactionPossible,
    JustOutput,
}
pub trait Simulator {
    //fn init(desc: interface::ModelDescription) -> Self;
    fn step(&mut self, step_limit: Option<f64>) -> StepResult;
    fn output_counts(&self) -> Vec<usize>;
    fn time(&self) -> f64;
    fn additional_int_stats(&self) -> serde_json::map::Map<String, serde_json::Value>;
}
