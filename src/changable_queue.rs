#[derive(Debug)]
pub struct ChangeableQueue<T> {
    pub data: Vec<T>,
    lookup: Vec<usize>,
}

pub trait QueueElement {
    fn give_idx(&self) -> usize;
}

impl<T> ChangeableQueue<T>
where
    T: Ord + QueueElement,
{
    pub fn new(max_len: usize) -> ChangeableQueue<T> {
        ChangeableQueue {
            data: vec![],
            lookup: vec![usize::MAX; max_len],
        }
    }

    pub fn peek(&self) -> Option<&T> {
        self.data.first()
    }

    pub fn push(&mut self, value: T) {
        assert_eq!(self.lookup[value.give_idx()], usize::MAX);
        self.data.push(value);
        let new_node_index = self.data.len() - 1;
        self.lookup[self.data[new_node_index].give_idx()] = new_node_index;
        self.sift_up(new_node_index);
    }

    pub fn pop(&mut self) -> Option<T> {
        if !self.data.is_empty() {
            //println!("popping vale at idx {}",self.data[0].give_idx());
            self.my_swap(0, self.data.len() - 1);
            let deleted_node = self.data.pop().unwrap();
            //assert!(self.data.iter().find(|k|**k == deleted_node).is_none());
            self.sift_down();
            self.lookup[deleted_node.give_idx()] = usize::MAX;
            //assert_eq!(self.lookup[deleted_node.give_idx()] , usize::MAX);
            Some(deleted_node)
        } else {
            None
        }
    }

    pub fn remove(&mut self, idx_in: usize) -> T {
        let mut idx = self.lookup[idx_in];
        //todo optimize
        while idx != 0 {
            let parent_index = self.parent_index(idx);
            self.my_swap(parent_index, idx);
            idx = parent_index;
        }
        assert_eq!(self.lookup[idx_in], 0);
        self.pop().unwrap()
        //self.check_heap();
        //assert!(self.data.iter().find(|k|**k == r).is_none());
        //r
    }
    pub fn change_priority(&mut self, new_val: T) {
        let idx = self.lookup[new_val.give_idx()];
        if new_val > self.data[idx] {
            self.data[idx] = new_val;
            self.sift_up(idx);
        } else if new_val < self.data[idx] {
            self.data[idx] = new_val;
            self.sift_down_at(idx);
        }
        //self.check_heap();
    }

    pub fn _check_heap(&self) {
        assert!(self.data.iter().max() == self.peek());
        for i in 1..self.data.len() {
            assert!(
                self.data[self.parent_index(i)] > self.data[i],
                "{} > {} @ {}",
                self.data[self.parent_index(i)].give_idx(),
                self.data[i].give_idx(),
                i
            );
        }
        for (n, v) in self.data.iter().enumerate() {
            assert_eq!(self.lookup[v.give_idx()], n, "Lookup: {:?}", self.lookup);
        }
    }

    fn my_swap(&mut self, idx_a: usize, idx_b: usize) {
        self.data.swap(idx_a, idx_b);
        self.lookup[self.data[idx_a].give_idx()] = idx_a;
        self.lookup[self.data[idx_b].give_idx()] = idx_b;
        //self.lookup.swap(self.data[idx_a].give_idx(),self.data[idx_b].give_idx());
    }

    fn sift_up(&mut self, mut new_node_index: usize) {
        while !self.is_root(new_node_index) && self.is_greater_than_parent(new_node_index) {
            let parent_index = self.parent_index(new_node_index);
            self.my_swap(parent_index, new_node_index);
            new_node_index = self.parent_index(new_node_index);
        }
    }

    fn is_root(&self, node_index: usize) -> bool {
        node_index == 0
    }

    fn is_greater_than_parent(&self, node_index: usize) -> bool {
        let parent_index = self.parent_index(node_index);
        self.data[node_index] > self.data[parent_index]
    }

    fn sift_down_at(&mut self, mut sifted_down_node_index: usize) {
        while self.has_greater_child(sifted_down_node_index) {
            let larger_child_index = self.calculate_larger_child_index(sifted_down_node_index);
            self.my_swap(sifted_down_node_index, larger_child_index);
            sifted_down_node_index = larger_child_index;
        }
    }
    fn sift_down(&mut self) {
        self.sift_down_at(0);
    }

    fn left_child_index(&self, index: usize) -> usize {
        (index * 2) + 1
    }

    fn right_child_index(&self, index: usize) -> usize {
        (index * 2) + 2
    }

    fn parent_index(&self, index: usize) -> usize {
        (index - 1) / 2
    }

    fn has_greater_child(&self, index: usize) -> bool {
        let left_child_index = self.left_child_index(index);
        let right_child_index = self.right_child_index(index);

        self.data.get(left_child_index).is_some() && self.data[left_child_index] > self.data[index]
            || self.data.get(right_child_index).is_some()
                && self.data[right_child_index] > self.data[index]
    }

    fn calculate_larger_child_index(&self, index: usize) -> usize {
        let left_child_index = self.left_child_index(index);
        let right_child_index = self.right_child_index(index);

        let left_child = self.data.get(left_child_index);
        let right_child = self.data.get(right_child_index);

        if ((right_child.is_some() && left_child.is_some()) && right_child > left_child)
            || left_child.is_none()
        {
            right_child_index
        } else {
            left_child_index
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::cmp::Ordering;

    struct I32Elem {
        val: i32,
        idx: usize,
    }

    impl Eq for I32Elem {}

    impl PartialEq<Self> for I32Elem {
        fn eq(&self, other: &Self) -> bool {
            self.val.eq(&other.val)
        }
    }

    impl QueueElement for I32Elem {
        fn give_idx(&self) -> usize {
            self.idx
        }
    }

    impl PartialOrd<Self> for I32Elem {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            self.val.partial_cmp(&other.val)
        }
    }

    impl std::cmp::Ord for I32Elem {
        fn cmp(&self, other: &Self) -> Ordering {
            self.val.cmp(&other.val)
        }
    }

    /*#[test]
    fn push_test() {
        let mut heap: ChangeableQueue<i32> = ChangeableQueue::new();
        heap.push(3);
        heap.push(2);

        assert_eq!(&vec![3, 2], heap.as_vec());
    }*/

    #[test]
    fn root_node_is_always_the_biggest_element_in_heap_after_push_test() {
        let mut heap: ChangeableQueue<I32Elem> = ChangeableQueue::new(5);
        heap._check_heap();
        heap.push(I32Elem { val: 5, idx: 0 });
        heap._check_heap();
        heap.push(I32Elem { val: 10, idx: 1 });
        heap._check_heap();
        heap.push(I32Elem { val: 2, idx: 2 });

        heap._check_heap();

        assert_eq!(10, heap.peek().unwrap().val);

        heap.push(I32Elem { val: 3, idx: 3 });
        heap._check_heap();

        assert_eq!(10, heap.peek().unwrap().val);

        heap.push(I32Elem { val: 20, idx: 4 });
        heap._check_heap();

        assert_eq!(20, heap.peek().unwrap().val);

        heap.pop();
        heap._check_heap();
        assert_eq!(10, heap.peek().unwrap().val);
        heap.pop();
        heap._check_heap();
        assert_eq!(5, heap.peek().unwrap().val);
    }

    #[test]
    fn heap_w_remove() {
        let mut heap: ChangeableQueue<I32Elem> = ChangeableQueue::new(5);
        heap._check_heap();
        heap.push(I32Elem { val: 5, idx: 0 });
        heap._check_heap();
        heap.push(I32Elem { val: 10, idx: 1 });
        heap._check_heap();
        heap.push(I32Elem { val: 2, idx: 2 });

        heap._check_heap();

        assert_eq!(10, heap.peek().unwrap().val);

        heap.push(I32Elem { val: 3, idx: 3 });
        heap._check_heap();
        assert_eq!(heap.remove(3).val, 3);
        heap._check_heap();

        assert_eq!(10, heap.peek().unwrap().val);

        heap.push(I32Elem { val: 20, idx: 4 });
        heap._check_heap();

        assert_eq!(20, heap.peek().unwrap().val);

        assert_eq!(heap.remove(4).val, 20);
        heap._check_heap();
        assert_eq!(10, heap.peek().unwrap().val);
        heap.pop();
        heap._check_heap();
        assert_eq!(5, heap.peek().unwrap().val);
    }

    /*#[test]
    fn pop_always_pop_the_root_node() {
        let mut heap: ChangeableQueue<i32> = ChangeableQueue::new();
        heap.push(10);
        heap.push(4);
        heap.push(7);

        heap.pop();

        assert!(!heap.as_vec().contains(&10));
    }*/

    /*#[test]
    fn pop_returns_a_variant_of_the_option_enum() {
        let mut heap: ChangeableQueue<i32> = ChangeableQueue::new();
        heap.push(10);

        assert_eq!(Some(10), heap.pop());
        assert_eq!(None, heap.pop());
    }

    #[test]
    fn root_node_is_always_the_biggest_element_in_heap_after_pop_test() {
        let mut heap: ChangeableQueue<i32> = ChangeableQueue::new();
        heap.push(5);
        heap.push(10);
        heap.push(2);

        heap.pop();

        assert_eq!(&5, heap.peek());

        heap.pop();

        assert_eq!(&2, heap.peek());
    }*/

    /*#[test]
    fn heap_is_generic_over_some_type_t() {
        let mut heap: ChangeableQueue<(i32, String)> = ChangeableQueue::new();
        heap.push((2, String::from("Zanzibar")));
        heap.push((10, String::from("Porto")));
        heap.push((5, String::from("Beijing")));

        let element = heap.pop();

        assert_eq!(Some((10, String::from("Porto"))), element);
        assert_eq!((5, String::from("Beijing")), heap.data[0]);
    }*/
}
