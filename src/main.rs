extern crate core;

mod changable_queue;
mod interface;
mod methods;

use crate::methods::{Method, Simulator, StepResult};
use clap::Parser;
use clap::Subcommand;
use itertools::Itertools;
use std::fs;
use std::time::Instant;

#[derive(Parser, Debug, Clone)]
#[command(author, version, about, long_about = None)]
struct Args {
    //#[arg(short, long,required)]
    filename: String,

    //#[arg(short, long, )]
    endtime: f64,

    #[arg(short, long, default_value_t = false)]
    endtime_is_wallclock: bool,

    #[arg(long)]
    num_observations: Option<usize>,

    #[arg(long,value_enum,default_value_t = Method::Mlrules)]
    method: Method,

    #[arg(long, default_value_t = false)]
    extra_stats: bool,

    #[command(subcommand)]
    special_commands: Option<NsmCommands>,
}

#[derive(Subcommand, Debug, Clone)]
enum NsmCommands {
    /// create NSM model
    MakeNsm {
        /// lists test values
        //#[arg(short, long)]
        gridsize: usize,
        diffusion: f64,
    },
}

/*#[derive(Serialize)]
struct Report {
    init_time: f64,
    exec_time: f64,
    time_measurements: Option<usize>,
    attempted_steps: usize,
    step_num: usize,
    sim_endtime: f64,
}*/

fn main() {
    let args: Args = Args::parse();
    let init_measure = Instant::now();

    let mut model = interface::ModelDescription::read_file(args.filename.to_string());
    match args.special_commands {
        None => {}
        Some(x) => match x {
            NsmCommands::MakeNsm {
                gridsize,
                diffusion,
            } => {
                let _species_vec: Vec<usize> = (0..model.initial_populations.len()).collect();
                model = model.make_next_subvolume(gridsize, Some(vec![2]), diffusion);
                let diff_str = format!("{diffusion}").replace(".", "-");
                let new_model_file = args
                    .filename
                    .replace(".json", &*format!("_nrm_{}_{}.json", gridsize, diff_str));
                if !std::path::Path::new(&new_model_file).exists() {
                    std::fs::write(
                        new_model_file.clone(),
                        serde_json::to_string_pretty(&model).unwrap(),
                    )
                    .unwrap();
                }
                model = interface::ModelDescription::read_file(new_model_file);
            }
        },
    }

    match args.method {
        Method::CodeGen => {
            assert!(!args.endtime_is_wallclock);
            methods::code_gen::CodeGen::run(model, args.endtime, args.num_observations);
            return;
        }
        _ => {}
    }

    //let mut sim = methods::dm::DirectMethod::init(model);
    //let mut sim = methods::ml_rules::MLRulesMethod::init(model);
    let mut sim: Box<dyn Simulator> = match args.method {
        Method::Mlrules => Box::new(methods::ml_rules::MLRulesMethod::init(model)),
        Method::Nrm => Box::new(methods::nrm2::NextReactionMethod2::init(
            model,
            args.extra_stats,
        )),
        Method::Dm => Box::new(methods::dm::DirectMethod::init(model, false)),
        Method::DmGraph => Box::new(methods::dm::DirectMethod::init(model, true)),
        Method::CodeGen => unreachable!(),
    };
    if args.endtime_is_wallclock && args.num_observations.is_some() {
        panic!("You can not specify no endtime and have observations")
    }
    /*match (args.nrm, args.nrm_reject) {
        (true, true) => panic!("can not select two methods"),
        (true, false) => ,
        (false, true) => Box::new(methods::nrm::NextReactionMethod::_init(model)),
        (false, false) => ,
    };*/
    //let mut sim =

    let mut step_count = 0;
    let mut count_time_measurements = 0;
    let mut next_timestep_check = 1000;
    let mut wallclock_done = false;
    let mut attempt_count = 0;
    let mut observations: Vec<(f64, Vec<usize>)> = vec![];
    let init_time = init_measure.elapsed().as_secs_f64();
    println!("Sim start");
    let start = Instant::now();
    while !wallclock_done
        && ((!args.endtime_is_wallclock && sim.time() < args.endtime)
            || (args.endtime_is_wallclock))
    {
        attempt_count += 1;
        let steplim = if args.endtime_is_wallclock {
            None
        } else {
            match args.num_observations {
                None => Some(args.endtime),
                Some(x) => Some(
                    args.endtime
                        .min(args.endtime / x as f64 * observations.len() as f64),
                ),
            }
        };

        match sim.step(steplim) {
            StepResult::Ok => step_count += 1,
            StepResult::NoReactionPossible => {
                println!("No reaction possible at t = {}", sim.time());
                if args.endtime_is_wallclock {
                    panic!("no reac possible");
                }
                break;
            }
            StepResult::JustOutput => {
                if args.num_observations.is_some() {
                    observations.push((sim.time(), sim.output_counts()));
                }
            }
        }
        if args.endtime_is_wallclock && (attempt_count >= next_timestep_check) {
            let elapsed = start.elapsed().as_secs_f64();
            count_time_measurements += 1;
            if elapsed > args.endtime {
                println!("done at {}", sim.time());
                wallclock_done = true;
            }
            let percantage_done = elapsed / args.endtime;
            let desired_next_percentage = ((percantage_done + 0.01) * 100.).ceil() / 100.;
            assert!(percantage_done < desired_next_percentage);
            next_timestep_check =
                (attempt_count as f64 / percantage_done * desired_next_percentage).ceil() as usize;
            println!("current: {} next {}", attempt_count, next_timestep_check);
            //println!("ellapsed : {}" ,start.elapsed().as_secs_f64());
        }
    }
    if observations.len() > 0 {
        println!("Writing output file");
        fs::write(
            "minimal_ssa_output.csv",
            observations
                .into_iter()
                .map(|(t, k)| format!("{t},") + &*k.into_iter().join(","))
                .join("\n"),
        )
        .expect("Could not write file");
    }
    let mut report = serde_json::json!( {
        "init_time" : init_time  ,
        "exec_time": start.elapsed().as_secs_f64(),
        "time_measurements": if args.endtime_is_wallclock {
            Some(count_time_measurements)
        } else {
            None
        },
        "attempted_steps": attempt_count,
        "step_num": step_count,
        "sim_endtime": sim.time(),
    });
    for (k, v) in sim.additional_int_stats() {
        report[k] = v;
    }

    println!("{}", serde_json::to_string(&report).unwrap())
}
